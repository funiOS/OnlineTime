
  Pod::Spec.new do |s|
  s.name         = "OnlineTime"
  s.version      = "0.1.0"
  s.summary      = "OnlineTime."
  s.homepage     = "https://gitee.com/xuanxiuiOS/OnlineTime"
  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "fun." => "yupeng_ios@163.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitee.com/xuanxiuiOS/OnlineTime.git", :tag => s.version.to_s }
  s.source_files  = "OnlineTime/OnlineTime/**/*.{h,m}"
  s.resources = 'OnlineTime/OnlineTime/Resources/*.{xib,nib,png}'
  s.dependency 'XLBaseModule'
  s.requires_arc = true
end
