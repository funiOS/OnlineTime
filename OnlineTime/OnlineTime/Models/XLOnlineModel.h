//
//  XLOnlineModel.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <XLBaseModule/XLBaseModel.h>

@interface XLOnlineModel : XLBaseModel
@property (nonatomic, assign, getter=isSelected)BOOL selected;
@property (nonatomic, copy)NSString *date;//日期
@property (nonatomic, copy)NSString *dateID;
- (instancetype)initWithDate:(NSString *)date;
+ (instancetype)modelWithDate:(NSString *)date;

@end

/** 在线时间*/
@interface XLOnlineTimeModel : XLOnlineModel
@property (nonatomic, copy)NSString *beginTime;//开始时间
@property (nonatomic, copy)NSString *endTime;//结束时间
//@property (nonatomic, assign, getter=isEditing)BOOL selected;

+ (instancetype)modelWithBeginTime:(NSString *)beginTime endTime:(NSString *)endTime date:(NSString *)date;
@end

/** 在线日*/
@interface XLOnlineDateModel : XLOnlineModel
//+(instancetype)modelWithDate:(NSString *)date;
@property (nonatomic, strong)NSMutableArray *dateArr;
@end
