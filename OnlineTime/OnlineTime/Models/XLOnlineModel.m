//
//  XLOnlineDateModel.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineModel.h"

@implementation XLOnlineModel
- (instancetype)initWithDate:(NSString *)date {
    self = [super init];
    if (self) {
        self.date = date;
        self.selected = NO;
    }
    return self;
}

+ (instancetype)modelWithDate:(NSString *)date {
    return [[self alloc]initWithDate:date];
}

@end

@implementation XLOnlineTimeModel
- (instancetype)initWithBeginTime:(NSString *)beginTime endTime:(NSString *)endTime date:(NSString *)date{
    self = [super initWithDate:date];
    if (self) {
        self.beginTime = beginTime;
        self.endTime = endTime;
    }
    return self;
}
+ (instancetype)modelWithBeginTime:(NSString *)beginTime endTime:(NSString *)endTime date:(NSString *)date{
    return [[self alloc]initWithBeginTime:beginTime endTime:endTime date:date];
}

@end

@implementation XLOnlineDateModel
//- (instancetype)initWithDate:(NSString *)date {
//    self = [super init];
//    if (self) {
//        self.date = date;
//        self.selected = NO;
//    }
//    return self;
//}
//+ (instancetype)modelWithDate:(NSString *)date {
//    return [[self alloc]initWithDate:date];
//}
//- (instancetype)initWithDate:(NSString *)date dateID:(NSString *)dateID {
//    self = [super init];
//    if (self) {
//        self.date = date;
//        self.dateID = dateID;
//        self.selected = NO;
//    }
//    return self;
//}
- (NSMutableArray *)dateArr {
    if (!_dateArr) {
        _dateArr = [NSMutableArray array];
//        NSArray *dateStrArr = @[@"每周日",@"每周一",@"每周二",@"每周三",@"每周四",@"每周五",@"每周六"];
        NSArray *dateStrArr = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
        for (int i = 0; i< dateStrArr.count; i++) {
            XLOnlineDateModel *model = [XLOnlineDateModel modelWithDate:dateStrArr[i]];
            if (i == 0) {
                model.dateID = @"7";
            } else {
                model.dateID = [NSString stringWithFormat:@"%d",i];
            }
            [_dateArr addObject:model];
        }
    }
    return _dateArr;
}


@end
