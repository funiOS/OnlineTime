//
//  XLOnlineTimeViewController.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineTimeViewController.h"
#import "XLOnlineTimeTableViewCell.h"
#import "XLOnlineTimeHeaderView.h"
#import "XLHandleOnlineTimeViewController.h"
//#import <Main_Category/Lothar+Main.h>
#import "Lothar+Main.h"
#import "XLOnlineModel.h"
@interface XLOnlineTimeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong)XLOnlineTimeHeaderView *headerView;
@property(nonatomic, strong)NSMutableArray *dataSource;

@end

@implementation XLOnlineTimeViewController
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //界面完全消失时 取消编辑操作
    [self.tableView setEditing:NO animated:YES];
    self.headerView.editing = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"在线时间";
    
    
    self.tableView.allowsSelectionDuringEditing = YES;//设置编辑状态下可以点击
    [self.tableView registerNib:[XLOnlineTimeTableViewCell xl_nib] forCellReuseIdentifier:[XLOnlineTimeTableViewCell identifier]];

}

- (void)initData {
    
    for (int i = 0; i<10; i++) {
        NSString *beginStr = [NSString stringWithFormat:@"08: %d",2*i+10];
        NSString *endStr = [NSString stringWithFormat:@"13: %d",2*i+10];
        XLOnlineTimeModel *model = [XLOnlineTimeModel modelWithBeginTime:beginStr endTime:endStr date:@"周一"];
        model.selected = NO;
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}
- (void)initInterface {
    self.headerView = [[XLOnlineTimeHeaderView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 60)];
    self.tableView.tableHeaderView = self.headerView;
    XLWeakSelf(self);
    self.headerView.block = ^(NSInteger tag,BOOL isEditing) {
        
        XLHandleOnlineTimeViewController *viewController = [[XLHandleOnlineTimeViewController alloc]init];
        if (tag == 0) {
            XLLog(@"编辑");
            viewController.type = XLHandleTypeEdit;

            [weakself.tableView setEditing:isEditing animated:YES];
        } else {
            XLLog(@"增加");
            viewController.type = XLHandleTypeAdd;
            UINavigationController *nav = [[Lothar shared]main_navBarViewContoller:viewController];
            [weakself presentViewController:nav animated:YES completion:nil];
        }
        
    };
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    XLOnlineTimeTableViewCell *cell = (XLOnlineTimeTableViewCell *)[XLOnlineTimeTableViewCell cellWithIdentifier:[XLOnlineTimeTableViewCell identifier] tableView:tableView indexPath:indexPath];
    cell.indexPath = indexPath;
    [cell setupDataModel:self.dataSource[indexPath.row]];
    
    XLWeakSelf(self);
    XLOnlineTimeModel *model = self.dataSource[indexPath.row];
    cell.switchBlock = ^(NSIndexPath *indexPath, BOOL isSelected) {
        model.selected = isSelected;
        [weakself.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    };
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.headerView.isEditing) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        XLLog(@"编辑");
        XLHandleOnlineTimeViewController *viewController = [[XLHandleOnlineTimeViewController alloc]init];
            viewController.type = XLHandleTypeEdit;
        UINavigationController *nav = [[Lothar shared]main_navBarViewContoller:viewController];

        [self presentViewController:nav animated:YES completion:nil];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
#pragma mark - Getter
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
