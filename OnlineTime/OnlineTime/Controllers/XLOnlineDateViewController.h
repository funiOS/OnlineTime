//
//  XLOnlineDateViewController.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <XLBaseModule/XLBaseViewController.h>
//#import "XLBaseViewController.h"
typedef void(^selectDateResultBlock)(NSString *selectDate);
@interface XLOnlineDateViewController : XLBaseViewController

@property (nonatomic, copy)selectDateResultBlock resultBlock;
//- (void)getSelectDate:(void(^)(NSString *selectValue))resultBlock;

@end
