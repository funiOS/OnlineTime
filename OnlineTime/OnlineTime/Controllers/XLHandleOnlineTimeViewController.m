//
//  XLHandleOnlineTimeViewController.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLHandleOnlineTimeViewController.h"
#import "XLOnlineTimeDatePickerView.h"
#import "XLOnlineDateViewController.h"
#import "UIView+Frame.h"
#import "UIView+XLVisuals.h"
//#import <BaseModule/UIView+Frame.h>
//#import <BaseModule/UIView+XLVisuals.h>
static const CGFloat kDatePickerViewHeight = 200;
@interface XLHandleOnlineTimeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)XLOnlineTimeDatePickerView *datePicker;
@end

@implementation XLHandleOnlineTimeViewController
{
    NSString *_onlineDate;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self.view addSubview:self.datePicker];
    self.title = @"编辑时间";
    _onlineDate = @"未选择";
    self.tableView.contentInset = UIEdgeInsetsMake(kDatePickerViewHeight, 0, 0, 0);
    [self.tableView addSubview:self.datePicker];
    if (self.type == XLHandleTypeEdit) {
        self.tableView.tableFooterView = [self creatFooterView];
    }
}

- (void)initInterface {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancleAction:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"存储" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction:)];
//    self.rightImgBtn.hidden = NO;
//    [self.rightImgBtn setTitle:@"存储" forState:UIControlStateNormal];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UIView *topLine;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
//        UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
       topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
        topLine.backgroundColor = kXLSeperateLineColor;
        [cell.contentView addSubview:topLine];
        UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(cell.frame)-1, KScreenWidth, 1)];
        bottomLine.backgroundColor = kXLSeperateLineColor;
        [cell.contentView addSubview:bottomLine];
    }
    topLine.hidden = indexPath.row != 0 ? YES : NO;
    cell.textLabel.text = @"在线日";
//    cell.detailTextLabel.text = @"周一 周二 周三";
    cell.detailTextLabel.text = _onlineDate;

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XLOnlineDateViewController *viewController = [[XLOnlineDateViewController alloc]init];
    XLWeakSelf(self);
    viewController.resultBlock = ^(NSString *selectDate) {
        _onlineDate = selectDate;
        [weakself.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    };
    
    [self.navigationController pushViewController:viewController animated:YES];
}
#pragma mark - Action 
- (void)cancleAction:(UIBarButtonItem *)button {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//- (void)submitAction:(UIButton *)sender {
//    [self.datePicker2 didFinishSelectedDate:^(NSString *beginDateStr, NSString *endDateStr) {
//        XLLog(@"beginDateStr:%@\n beginDateStr:%@",beginDateStr,endDateStr);
//    }];
//}
- (void)saveAction:(UIBarButtonItem *)button {
    [self.datePicker didFinishSelectedDate:^(NSString *beginDateStr, NSString *endDateStr) {
        XLLog(@"beginDateStr:%@\n beginDateStr:%@",beginDateStr,endDateStr);
    }];
//    XLLog(@"%@%@",self.datePicker2.beginDateStr,self.datePicker2.endDateStr);
}

#pragma mark - Getter
- (XLOnlineTimeDatePickerView *)datePicker {
    if (!_datePicker) {
        _datePicker = [[XLOnlineTimeDatePickerView alloc]initWithFrame:CGRectMake(0, -kDatePickerViewHeight, KScreenWidth, kDatePickerViewHeight)];
    }
    return _datePicker;
}

#pragma mark - footer
- (UIView *)creatFooterView {
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-kDatePickerViewHeight-40-44)];
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setTitle:@"删除该时间" forState:UIControlStateNormal];
    CGSize buttonSize = CGSizeMake(KScreenWidth/2, 50);
    deleteButton.frame = CGRectMake((footerView.width-buttonSize.width)/2, footerView.height-200-buttonSize.height, buttonSize.width, buttonSize.height);
    [deleteButton xl_cornerRadius:buttonSize.height/2 strokeSize:1 color:kXLButtonColor_BLUE];
    deleteButton.backgroundColor = kXLButtonColor_BLUE;
    [footerView addSubview:deleteButton];
    return footerView;
}

- (void)initData {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
