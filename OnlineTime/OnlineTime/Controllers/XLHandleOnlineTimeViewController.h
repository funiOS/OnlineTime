//
//  XLHandleOnlineTimeViewController.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <XLBaseModule/XLBaseViewController.h>
//#import "XLBaseViewController.h"
typedef NS_ENUM(NSInteger, XLHandleOnlineTimeType){
    XLHandleTypeEdit = 0,//编辑
    XLHandleTypeAdd = 1  //增加
};
@interface XLHandleOnlineTimeViewController : XLBaseViewController
@property (nonatomic, assign)XLHandleOnlineTimeType type;
@end
