//
//  XLOnlineDateViewController.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineDateViewController.h"
#import "XLOnelineDateTableViewCell.h"
#import "XLOnlineModel.h"
@interface XLOnlineDateViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, strong)NSMutableArray *selectArr;//选中的数据
@property (nonatomic, copy)NSString *selectDate;

@end

@implementation XLOnlineDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"在线日";
    [self.tableView registerNib:[XLOnelineDateTableViewCell xl_nib] forCellReuseIdentifier:[XLOnelineDateTableViewCell identifier]];
}

- (void)initInterface {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 20)];
    self.tableView.tableHeaderView = headerView;
}
- (void)initData {
    
//    NSArray *dateStrArr = @[@"每周日",@"每周一",@"每周二",@"每周三",@"每周四",@"每周五",@"每周六"];
//    for (int i = 0; i< dateStrArr.count; i++) {
//        XLOnlineDateModel *model = [XLOnlineDateModel modelWithDate:dateStrArr[i]];
//        [self.dataSource addObject:model];
//    }
    XLOnlineDateModel *model = [[XLOnlineDateModel alloc]init];
    self.dataSource = model.dateArr;
    [self.tableView reloadData];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XLOnelineDateTableViewCell *cell = (XLOnelineDateTableViewCell *)[XLOnelineDateTableViewCell cellWithIdentifier:[XLOnelineDateTableViewCell identifier] tableView:tableView indexPath:indexPath];
    [cell setupDataModel:self.dataSource[indexPath.row]];
    cell.topLineView.hidden = indexPath.row != 0 ? YES : NO;
    cell.indexPath = indexPath;
    
//    XLOnlineDateModel *model = self.dataSource[indexPath.row];
//    XLWeakSelf(self);
//    cell.resultBlock = ^(NSIndexPath *indexPath) {
//        model.selected = !model.selected;
//        [weakself.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    };
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XLOnlineDateModel *model = self.dataSource[indexPath.row];
    model.selected = !model.selected;
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

    if (model.isSelected) {
        [self.selectArr addObject:model];
    } else {
        [self.selectArr removeObject:model];
    }
    NSMutableString *string = [NSMutableString string];
    if (self.selectArr.count == self.dataSource.count) {
        [string appendString:@"每天"];
    } else if (self.selectArr.count == 0) {
        [string appendString:@"永不"];
    } else {
        NSString *placeholderStr = @" ";
        NSArray *workDayIDs = @[@"1",@"2",@"3",@"4",@"5"];//工作日ID
        NSArray *weekendIDs = @[@"6",@"7"];//周末ID
        NSSet *workDaySet = [NSSet setWithArray:workDayIDs];
        NSSet *weekendSet = [NSSet setWithArray:weekendIDs];
        NSMutableArray *selectDayIDs = @[].mutableCopy;
        for (XLOnlineDateModel *model in self.selectArr) {
            [selectDayIDs addObject:model.dateID];
        }
        NSSet *selectSet = [NSSet setWithArray:selectDayIDs.copy];
        
        if ([selectSet isEqualToSet:workDaySet]) {
            [string appendString:@"工作日"];
        } else if([selectSet isEqualToSet:weekendSet]) {
            [string appendString:@"周末"];
        }  else {
            //其他
            for (XLOnlineDateModel *model in self.selectArr) {
                [selectDayIDs addObject:model.dateID];
                [string appendString:model.date];
                [string appendString:placeholderStr];
            }
        }
        
    }
   
    self.selectDate = string.copy;
    XLLog(@"选择的日期:%@",self.selectDate);
    if (self.resultBlock) {
        self.resultBlock(self.selectDate);
    }
//    NSMutableString *string = [NSMutableString string];
//    NSString *placeholderStr = @" ";
//    for (XLOnlineDateModel *model in self.dataSource) {
//        if (model.isSelected) {
//            [string appendString:model.date];
//            [string appendString:placeholderStr];
//        }
//    }
//    self.selectDate = string.copy;
//    XLLog(@"选择的日期:%@",self.selectDate);
//    if (self.resultBlock) {
//        self.resultBlock(self.selectDate);
//    }
    
}

//- (void)getSelectDate:(void (^)(NSString *))resultBlock {
//    resultBlock(self.selectDate);
//}
#pragma mark - Getter
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (NSMutableArray *)selectArr {
    if (!_selectArr) {
        _selectArr = [NSMutableArray array];
    }
    return _selectArr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
