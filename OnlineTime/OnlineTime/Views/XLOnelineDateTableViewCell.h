//
//  XLOnelineDateTableViewCell.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

//#import <XLBaseModule/XLBaseTableViewCell.h>
#import "XLBaseTableViewCell.h"
typedef void(^selectResultBlock)(NSIndexPath *indexPath);
@interface XLOnelineDateTableViewCell : XLBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIView *topLineView;
@property (nonatomic, strong)NSIndexPath *indexPath;
//@property (nonatomic, copy)selectResultBlock resultBlock;
@end
