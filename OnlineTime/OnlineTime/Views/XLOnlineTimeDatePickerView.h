//
//  XLOnlineTimeDatePickerView.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^XLOnlineTimeDateResultBlock)(NSString *beginDateStr, NSString *endDateStr);
@interface XLOnlineTimeDatePickerView : UIView

- (void)didFinishSelectedDate:(XLOnlineTimeDateResultBlock)selectResult;

@end
