//
//  XLOnlineTimeDatePickerView.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineTimeDatePickerView.h"
#import "XLDatePickerView.h"
//#import <XLBaseModule/XLAppMacro.h>
//#import <Masonry.h>
#import "XLAppMacro.h"
#import "Masonry.h"
#define kBreakLineBackgoundColor kRGB(212,212,212)
static CGFloat kMinTimeInterval = 10*60;//最小时间间隔(单位:秒)
static const CGFloat kBreakLineUnitWidth = 1.0f;
static const CGFloat kBreakLineUnitHeight = 1.0f;
static const CGFloat kViewSpace = 30;

@interface XLOnlineTimeDatePickerView ()
/** 起始时间*/
@property (nonatomic, strong) XLDatePickerView *beginPickerView;
/** 结束时间*/
@property (nonatomic, strong) XLDatePickerView *endPickerView;
/** 分割线*/
@property (nonatomic, strong) UIView *breakLine;
/** 起始分割线*/
@property (nonatomic, strong) UIView *beginBreakLine;
/** 结束分割线*/
@property (nonatomic, strong) UIView *endBreakLine;
/** 底部分割线*/
@property (nonatomic, strong) UIView *bottomBreakLine;

//@property (nonatomic, copy)XLOnlineTimeDateResultBlock resultBlock;
@property (nonatomic, copy)NSString *beginDateStr;
@property (nonatomic, copy)NSString *endDateStr;
@end

@implementation XLOnlineTimeDatePickerView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self configUI];
        [self addConstraints];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
        [self addConstraints];
    }
    return self;
}


- (void)configUI {
    [self addSubview:self.beginPickerView];
    [self addSubview:self.beginBreakLine];
    [self addSubview:self.breakLine];
    [self addSubview:self.endBreakLine];
    [self addSubview:self.endPickerView];
    [self addSubview:self.bottomBreakLine];
    
    self.beginDateStr = self.beginPickerView.selectDateStr;
    self.endDateStr = self.endPickerView.selectDateStr;


    XLWeakSelf(self);

    [self.beginPickerView didFinishSelectedDate:^(NSString *dateValue) {
        
        //设置结束时间最小值=起始时间+间隔
        NSDate *date = [self.beginPickerView toDateWithDateString:dateValue];
        NSDate *afterDate = [NSDate dateWithTimeInterval:kMinTimeInterval sinceDate:date];
        NSString *afterStr = [self.beginPickerView toStringWithDate:afterDate];
        
        weakself.endPickerView.minDateStr = afterStr;
        weakself.endDateStr = afterStr;//更新结束时间值
        weakself.beginDateStr = dateValue;

    }];
    
    [self.endPickerView didFinishSelectedDate:^(NSString *dateValue) {
        weakself.endDateStr = dateValue;
    }];
    
}

- (void)addConstraints {
    [self.beginPickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(0);
        make.bottom.equalTo(self.bottomBreakLine.mas_top).offset(0);
        make.width.equalTo(self.mas_width).offset(-kViewSpace).multipliedBy(.5);
    }];
    [self.beginBreakLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.beginPickerView.mas_right).offset(0);
        make.top.equalTo(self).offset(0);
        make.bottom.equalTo(self.beginPickerView);
        make.width.mas_equalTo(kBreakLineUnitWidth);
    }];
    [self.breakLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self).offset(0);
        make.width.mas_equalTo(kViewSpace-5);
        make.height.mas_equalTo(2*kBreakLineUnitHeight);
    }];
    [self.endPickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.equalTo(self).offset(0);
        make.bottom.equalTo(self.beginPickerView);
        make.width.equalTo(self.beginPickerView);
    }];
    [self.endBreakLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(0);
        make.right.equalTo(self.endPickerView.mas_left).offset(0);
        make.bottom.equalTo(self.beginPickerView);
        make.width.mas_equalTo(kBreakLineUnitWidth);
    }];
    [self.bottomBreakLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self).offset(0);
        make.height.mas_equalTo(kBreakLineUnitHeight);

    }];
}

#pragma mark - Method
- (void)didFinishSelectedDate:(XLOnlineTimeDateResultBlock)selectResult {
//     self.resultBlock = selectDateTime;
    selectResult(self.beginDateStr,self.endDateStr);
}
#pragma mark - Getter
- (XLDatePickerView *)beginPickerView {
    if (!_beginPickerView) {
        _beginPickerView = [[XLDatePickerView alloc]init];
        
    }
    return _beginPickerView;
}

- (XLDatePickerView *)endPickerView {
    if (!_endPickerView) {
        _endPickerView = [[XLDatePickerView alloc]init];
    }
    return _endPickerView;
}

- (UIView *)beginBreakLine {
    if (!_beginBreakLine) {
        _beginBreakLine = [[UIView alloc]init];
        _beginBreakLine.backgroundColor = kBreakLineBackgoundColor;
    }
    return _beginBreakLine;
}

- (UIView *)endBreakLine {
    if (!_endBreakLine) {
        _endBreakLine = [[UIView alloc]init];
        _endBreakLine.backgroundColor = kBreakLineBackgoundColor;
    }
    return _endBreakLine;
}

- (UIView *)breakLine {
    if (!_breakLine) {
        _breakLine = [[UIView alloc]init];
        _breakLine.backgroundColor = kBreakLineBackgoundColor;
    }
    return _breakLine;
}

- (UIView *)bottomBreakLine {
    if (!_bottomBreakLine) {
        _bottomBreakLine = [[UIView alloc]init];
        _bottomBreakLine.backgroundColor = kBreakLineBackgoundColor;
    }
    return _bottomBreakLine;
}
@end
