//
//  XLOnlineTimeTableViewCell.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineTimeTableViewCell.h"
#import "XLOnlineModel.h"
@interface XLOnlineTimeTableViewCell ()
@property (weak, nonatomic) IBOutlet XLCellLabel *timeLabel;
@property (weak, nonatomic) IBOutlet XLCellLabel *weekLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchItem;

@end

@implementation XLOnlineTimeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (XLBaseTableViewCell *)cellWithIdentifier:(NSString *)cellIdentifier tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    XLOnlineTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    return cell;
}

- (void)setupUI {
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.weekLabel.textColorType = XLLabelTextColorGray;
    self.weekLabel.fontType = XLLabelFontDetail;
}

- (void)setupDataModel:(XLBaseCellModel *)model {
    [super setupDataModel:model];
    XLOnlineTimeModel *timeModel = (XLOnlineTimeModel *)model;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ —— %@",timeModel.beginTime,timeModel.endTime];
    self.weekLabel.text = timeModel.date;
    self.switchItem.on = timeModel.isSelected;
}
- (IBAction)switchValueChange:(UISwitch *)sender {
    sender.on = !sender.on;
    if (self.switchBlock) {
        self.switchBlock(self.indexPath, sender.isOn);
    }
}
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    self.switchItem.hidden = editing;
    self.selectionStyle = editing ? UITableViewCellSelectionStyleDefault : UITableViewCellSelectionStyleNone;
//    self.accessoryType = editing ?UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
