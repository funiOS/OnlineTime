//
//  XLOnlineTimeHeaderView.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnlineTimeHeaderView.h"
//#import <BaseModule/UIView+XLVisuals.h>
//#import <XLBaseModule/XLAppMacro.h>
//#import <Masonry.h>
#import "UIView+XLVisuals.h"
#import "XLAppMacro.h"
#import "Masonry.h"
@interface XLOnlineTimeHeaderView ()
@property (nonatomic, strong)UIButton *editButton;
@property (nonatomic, strong)UIButton *addButton;
@property (nonatomic, strong)UIView *lineView;
@end

@implementation XLOnlineTimeHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)configUI {
    self.editButton = [self createButtonWithTitle:@"编辑" tag:0];
    self.addButton = [self createButtonWithTitle:@"增加" tag:1];
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = kXLSeperateLineColor;
    [self addSubview:lineView];
    self.lineView = lineView;
    
    [self addConstraints];
}
- (void)addConstraints {
//    CGFloat margin = 30;
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(0);
//        make.right.equalTo(self.mas_centerX).offset(-margin/2);
        make.left.equalTo(self.mas_right).multipliedBy(1.0/9);
        make.width.equalTo(self.mas_width).multipliedBy(1.0/3);
        make.height.equalTo(self.mas_height).multipliedBy(3.0/5);
    }];
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(0);
        //        make.right.equalTo(self.mas_centerX).offset(-margin/2);
        make.right.equalTo(self.mas_right).multipliedBy(8.0/9);
        make.size.equalTo(self.editButton);
//        make.width.equalTo(self.mas_width).multipliedBy(1.0/3);
//        make.height.equalTo(self.mas_height).multipliedBy(3.0/5);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(0);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(kXLSeperateLineHeight);
    }];
    
}

- (void)clickAction:(UIButton *)sender {
    if (sender.tag == kXLBasicTag) {
        self.editing = !self.editing;
//        if (self.isEditing) {
//            [sender setTitle:@"完成" forState:UIControlStateNormal];
//        } else {
//            [sender setTitle:@"编辑" forState:UIControlStateNormal];
//
//        }
    }
//    if (self.block) {
//        self.block(sender.tag-kXLBasicTag);
//    }
    if (self.block) {
        self.block(sender.tag-kXLBasicTag, self.isEditing);
    }
}

- (UIButton *)createButtonWithTitle:(NSString *)title tag:(NSInteger)tag{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:kXLTextColor_BLACK forState:UIControlStateNormal];
    [button xl_cornerRadius:10 strokeSize:1 color:kXLBlueColor];
    button.tag = tag+kXLBasicTag;
    [button addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    return button;
}

#pragma mark - Setter
- (void)setEditing:(BOOL)editing {
    _editing = editing;
    [self.editButton setTitle:editing ? @"完成" : @"编辑" forState:UIControlStateNormal];
}
@end
