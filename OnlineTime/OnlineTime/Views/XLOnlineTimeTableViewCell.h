//
//  XLOnlineTimeTableViewCell.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

//#import <XLBaseModule/XLBaseTableViewCell.h>
#import "XLBaseTableViewCell.h"
typedef void(^XLCellSwitchBlock)(NSIndexPath *indexPath, BOOL isSelected);
@interface XLOnlineTimeTableViewCell : XLBaseTableViewCell
@property (nonatomic, strong)NSIndexPath *indexPath;
@property (nonatomic, copy)XLCellSwitchBlock switchBlock;
@end
