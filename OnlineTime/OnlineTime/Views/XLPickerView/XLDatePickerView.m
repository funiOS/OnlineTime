//
//  XLDatePickerView.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLDatePickerView.h"

@interface XLDatePickerView()
// 时间选择器(默认大小: 320px × 216px)
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, copy)XLDateResultBlock resultBlock;

@end

@implementation XLDatePickerView

- (void)dealloc {
//    [self removeObserver:self forKeyPath:@"selectDateStr"];
}

#pragma mark - 初始化时间选择器
- (instancetype)init {
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
    }
    return self;
}


- (void)configUI {
    self.datePickerMode = UIDatePickerModeTime;
    // 默认选中今天的日期
    self.selectDateStr = [self toStringWithDate:[NSDate date]];
    
    [self addSubview:self.datePicker];
//    [self addObserver:self forKeyPath:@"selectDateStr" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.datePicker.frame = self.bounds;
}

#pragma mark - Action 

- (void)didSelectValueChanged:(UIDatePicker *)sender {
    self.selectDateStr = [self toStringWithDate:sender.date];
    if (self.resultBlock) {
        self.resultBlock(self.selectDateStr);
    }
}
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
//    if ([keyPath isEqualToString:@"selectDateStr"]) {
//        NSString *newValue = change[@"new"];
////        NSLog(@"newValue:%@",newValue);
//        if (self.resultBlock) {
//            self.resultBlock(newValue);
//        }
//    }
//}
#pragma mark - Setter
- (void)setDatePickerMode:(UIDatePickerMode)datePickerMode {
    _datePickerMode = datePickerMode;
    self.datePicker.datePickerMode = datePickerMode;
}

- (void)setMinDateStr:(NSString *)minDateStr {
    _minDateStr = minDateStr;
    self.datePicker.minimumDate = [self toDateWithDateString:minDateStr];
}

- (void)setMaxDateStr:(NSString *)maxDateStr {
    _maxDateStr = maxDateStr;
    self.datePicker.maximumDate = [self toDateWithDateString:maxDateStr];
}

- (void)setSelectDateStr:(NSString *)selectDateStr {
    _selectDateStr = selectDateStr;
    self.datePicker.date = [self toDateWithDateString:selectDateStr];
}
#pragma mark - 时间选择器
- (UIDatePicker *)datePicker {
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc]init];
//        _datePicker.backgroundColor = [UIColor whiteColor];
        _datePicker.datePickerMode = _datePickerMode;
        // 设置该UIDatePicker的国际化Locale，以简体中文习惯显示日期，UIDatePicker控件默认使用iOS系统的国际化Locale
        _datePicker.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"zh_CHS_CN"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        // 设置时间范围
        if (_minDateStr) {
            NSDate *minDate = [formatter dateFromString:_minDateStr];
            _datePicker.minimumDate = minDate;
        }
        if (_maxDateStr) {
            NSDate *maxDate = [formatter dateFromString:_maxDateStr];
            _datePicker.maximumDate = maxDate;
        }
        
        // 把当前时间赋值给 _datePicker
        [_datePicker setDate:[NSDate date] animated:YES];
        
        // 滚动改变值的响应事件
        [_datePicker addTarget:self action:@selector(didSelectValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _datePicker;
}
#pragma mark - Method
- (void)didFinishSelectedDate:(XLDateResultBlock)selectDateTime {
    _resultBlock = selectDateTime;
}

#pragma mark  格式转换：NSDate --> NSString
- (NSString *)toStringWithDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    switch (_datePickerMode) {
        case UIDatePickerModeTime:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        case UIDatePickerModeDate:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            break;
        case UIDatePickerModeDateAndTime:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            break;
        case UIDatePickerModeCountDownTimer:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        default:
            break;
    }
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
}

#pragma mark  格式转换：NSDate <-- NSString
- (NSDate *)toDateWithDateString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    switch (_datePickerMode) {
        case UIDatePickerModeTime:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        case UIDatePickerModeDate:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            break;
        case UIDatePickerModeDateAndTime:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            break;
        case UIDatePickerModeCountDownTimer:
            [dateFormatter setDateFormat:@"HH:mm"];
            break;
        default:
            break;
    }
    NSDate *destDate = [dateFormatter dateFromString:dateString];
    
    return destDate;
}

@end
