//
//  XLDatePickerView.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^XLDateResultBlock)(NSString *dateValue);

@interface XLDatePickerView : UIView
/** UIDatePickerMode*/
@property (nonatomic, assign) UIDatePickerMode datePickerMode;
/** 最小时间(eg: 2017-08-28 14:57:00)*/
@property (nonatomic, copy) NSString *minDateStr;
/** 最大时间*/
@property (nonatomic, copy) NSString *maxDateStr;
/** 选中的时间(默认为当前时间)*/
@property (nonatomic, copy) NSString *selectDateStr;
//@property (nonatomic, copy)XLDateResultBlock resultBlock;
- (void)didFinishSelectedDate:(XLDateResultBlock)selectDateTime;
- (NSString *)toStringWithDate:(NSDate *)date;
- (NSDate *)toDateWithDateString:(NSString *)dateString;

@end
