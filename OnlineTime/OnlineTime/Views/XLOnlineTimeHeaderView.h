//
//  XLOnlineTimeHeaderView.h
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/28.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef void(^buttonClickBlock)(NSInteger tag);
typedef void(^buttonClickBlock)(NSInteger tag, BOOL isEditing);
@interface XLOnlineTimeHeaderView : UIView
@property (nonatomic, copy)buttonClickBlock block;
@property(nonatomic, assign, getter=isEditing)BOOL editing;//是否正在编辑

@end
