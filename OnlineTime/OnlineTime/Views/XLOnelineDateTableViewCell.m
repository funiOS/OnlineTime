//
//  XLOnelineDateTableViewCell.m
//  OnlineTime
//
//  Created by 于鹏 on 2017/8/29.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "XLOnelineDateTableViewCell.h"
#import "XLOnlineModel.h"
@interface XLOnelineDateTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet XLCellLabel *dateLabel;
//@property (weak, nonatomic) IBOutlet UIView *topLineView;

@end

@implementation XLOnelineDateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setupUI {
    
}
//- (IBAction)selectAction:(UIButton *)sender {
//    sender.selected = !sender.selected;
//    if (self.resultBlock) {
//        self.resultBlock(self.indexPath);
//    }
//}

- (void)setupDataModel:(XLBaseCellModel *)model {
    [super setupDataModel:model];
    XLOnlineDateModel *dateModel = (XLOnlineDateModel *)model;
//    self.dateLabel.text = dateModel.date;
    self.dateLabel.text = [NSString stringWithFormat:@"每%@",dateModel.date];
    self.selectBtn.selected = dateModel.isSelected;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
